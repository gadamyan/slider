#ifdef GL_ES
precision mediump int;
precision mediump float;
#endif

uniform mat4 mvp_matrix;

attribute vec4 a_position;

varying vec2 v_surfacePosition;

void main()
{
    gl_Position = mvp_matrix * a_position;
    v_surfacePosition = gl_Position.xy;
}
