#ifdef GL_ES
// Set default precision to medium
precision mediump int;
precision mediump float;
#endif

uniform float time;
uniform vec2 resolution;
varying vec2 v_surfacePosition;

#define MAX_ITER 4
void main() {
    vec2 sp = v_surfacePosition;
    vec2 p = sp * 5.0 - vec2(10.0);
    vec2 i = p;
    float c = 1.0;
    float inten = .1;

    for (int n = 0; n < MAX_ITER; n++)
    {
        float t = time * 0.0005 * (1.0 - (3.0 / float(n+1)));
        i = p + vec2(cos(t - i.x) + sin(t + i.y), sin(t - i.y) + cos(t + i.x));
        c += 1.0/length(vec2(p.x / (sin(i.x+t)/inten),p.y / (cos(i.y+t)/inten)));
    }
    c /= float(MAX_ITER);
    c = 1.5-sqrt(c);
    gl_FragColor = vec4(vec3(c * c * c * c), 999.0) + vec4(0.0, 0.3, 0.5, 1.0);
}
