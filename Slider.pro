#-------------------------------------------------
#
# Project created by QtCreator 2016-06-10T20:01:34
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Slider
TEMPLATE = app


SOURCES += Source/main.cpp\
    Source/slider.cpp \
    Source/slider3d.cpp \
    Source/windowconnector.cpp \
    Source/Core/glcompositenode.cpp \
    Source/Core/glnode.cpp \
    Source/Core/glscene.cpp \
    Source/Core/glshadersprite.cpp \
    Source/Core/glsphere.cpp \
    Source/Core/glsprite.cpp \
    Source/Core/glwidget.cpp \
    Source/Forms/inputsliderform.cpp \
    Source/Forms/mainwindow.cpp \
    Source/Scenes/customscene.cpp \
    Source/Scenes/firstscene.cpp \
    Source/Scenes/gameoverscene.cpp \
    Source/Scenes/secondscene.cpp \
    Source/Core/glshapegeometry.cpp \
    Source/Core/glrectgeometry.cpp \
    Source/Core/glspheregeometry.cpp \
    Source/Managers/settingsmanager.cpp

HEADERS  += \
    Source/slider.h \
    Source/slider3d.h \
    Source/windowconnector.h \
    Source/Core/glcompositenode.h \
    Source/Core/glnode.h \
    Source/Core/glscene.h \
    Source/Core/glshadersprite.h \
    Source/Core/glsphere.h \
    Source/Core/glsprite.h \
    Source/Core/glwidget.h \
    Source/Forms/inputsliderform.h \
    Source/Forms/mainwindow.h \
    Source/Scenes/customscene.h \
    Source/Scenes/firstscene.h \
    Source/Scenes/gameoverscene.h \
    Source/Scenes/secondscene.h \
    Source/Core/glshapegeometry.h \
    Source/Core/glrectgeometry.h \
    Source/Core/glspheregeometry.h \
    Source/Managers/settingsmanager.h \
    Source/Managers/settingsdata.h

FORMS    += mainwindow.ui \
    inputsliderform.ui

DISTFILES += \
    fshader.fsh \
    vshader.vsh

RESOURCES += \
    textures.qrc \
    shaders.qrc \
    jsons.qrc
