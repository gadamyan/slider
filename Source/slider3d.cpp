#include "slider3d.h"
#include <cmath>
#include <QtMath>

Slider3D::Slider3D()
{
    m_sliderThumb = std::make_shared<GLSprite>(":/Resources/sliderThumb.png");
    m_children.push_back(m_sliderThumb);

    //TODO remove magic numbers
    const float radius = 2.0f;
    const int sphereCount = 7;
    const float gapDegree = M_PI / (sphereCount - 1);

    for (int i = 0; i < 7; ++i)
    {
        const float angle = M_PI - i * gapDegree;
        const float x = radius * cos(angle) + m_position.x();
        const float y = radius * sin(angle) + m_position.y();

        auto sphere = std::make_shared<GLSphere>(0.2f, 6, ":/Resources/ball.jpg");
        sphere->setPosition(QVector3D(x, y, 0));
        m_children.push_back(sphere);
        m_spheres.push_back(sphere);
    }
}

void Slider3D::setValue(const int value)
{
    updateThumbPositionAndRotation(value);
    scaleBalls();
}

void Slider3D::updateThumbPositionAndRotation(const int value)
{
    const float angleInRadians = M_PI - M_PI * value/ 100.0f;
    const float radius = 2.5f;
    const float x = radius * cos(angleInRadians);
    const float y = radius * sin(angleInRadians);
    QVector3D thumbPosition(x, y, 0.0f);
    m_sliderThumb->setPosition(thumbPosition);

    const float angleInDegrees = qRadiansToDegrees(M_PI + angleInRadians);
    const QQuaternion rotation = QQuaternion::fromAxisAndAngle(QVector3D(0,0,1), angleInDegrees);
    m_sliderThumb->setRotation(rotation);
}

void Slider3D::scaleBalls()
{
    const QVector3D& thumbPosition = m_sliderThumb->getPosition();
    for (auto sphere : m_spheres)
    {
        float scale = 1.0f;
        const QVector3D& position = sphere->getPosition();
        float distance = position.distanceToPoint(thumbPosition);

        //TODO remove magic numbers and make it readable
        if (distance < 1.5f)
        {
            const float space = distance - 0.5f;
            scale = 1.0f + (1.0f - space) * 0.5f;
        }
        sphere->setScale(scale);
    }
}
