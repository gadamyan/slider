#include "Source/Forms/mainwindow.h"
#include "Source/windowconnector.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    WindowConnector connector;
    return a.exec();
}
