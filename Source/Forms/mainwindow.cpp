#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "Source/Scenes/firstscene.h"
#include "Source/Scenes/secondscene.h"
#include "Source/Scenes/gameoverscene.h"
#include <cassert>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
    , m_activeScene(ActiveScene::NoScene)
{
    ui->setupUi(this);
    m_glWidget = ui->glWidget;
    QObject::connect(m_glWidget, SIGNAL(glInitializedSignal()), this, SLOT(glInitializedSlot()));
}

void MainWindow::sliderValueChangedSlot(int value)
{
    //TODO design a better architecture and move the scene changing responsibility out of this class
    if (m_scene)
    {
        m_scene->sliderValueChanged(value);
        if (m_scene->shouldChangeScene())
        {
            selectScene();
        }
        activateWindow();
    }
}

void MainWindow::selectScene()
{
    switch(m_activeScene)
    {
    case ActiveScene::NoScene:
    {
        const SceneSettingsData& data = m_settingsManager.getFirstSceneSettingsData();
        changeScene(std::make_shared<FirstScene>(data), ActiveScene::FirstScene);
        break;
    }
    case ActiveScene::FirstScene:
    {
        const SceneSettingsData& data = m_settingsManager.getSecondSceneSettingsData();
        changeScene(std::make_shared<SecondScene>(data), ActiveScene::SecondScene);
        break;
    }
    case ActiveScene::SecondScene:
        changeScene(std::make_shared<GameOverScene>(), ActiveScene::GameOverScene);
        break;
    case ActiveScene::GameOverScene:
        assert(false && "Not Implemented");
    }
}

void MainWindow::changeScene(std::shared_ptr<CustomScene> scene, const ActiveScene activeScene)
{
    m_activeScene = activeScene;
    m_scene = scene;
    m_glWidget->setScene(m_scene);
}

void MainWindow::glInitializedSlot()
{
    selectScene();
}

MainWindow::~MainWindow()
{
    delete ui;
}
