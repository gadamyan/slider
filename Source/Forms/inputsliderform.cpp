#include "inputsliderform.h"
#include "ui_inputsliderform.h"

InputSliderForm::InputSliderForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InputSliderForm)
{
    ui->setupUi(this);
    connect(ui->verticalSlider, SIGNAL(sliderMoved(int)), this, SLOT(sliderValueChangedSlot(int)));
}

void InputSliderForm::sliderValueChangedSlot(int value)
{
    sliderValueChangedSignal(value);
}

InputSliderForm::~InputSliderForm()
{
    delete ui;
}
