#ifndef INPUTSLIDERFORM_H
#define INPUTSLIDERFORM_H

#include <QWidget>

namespace Ui {
class InputSliderForm;
}

class InputSliderForm : public QWidget
{
    Q_OBJECT

public:
    explicit InputSliderForm(QWidget *parent = 0);
    ~InputSliderForm();

public slots:
    void sliderValueChangedSlot(int value);

signals:
    void sliderValueChangedSignal(int value);

private:
    Ui::InputSliderForm *ui;
};

#endif // INPUTSLIDERFORM_H
