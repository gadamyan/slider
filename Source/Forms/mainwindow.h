#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "Source/Core/glwidget.h"
#include "Source/Scenes/customscene.h"
#include "Source/Managers/settingsmanager.h"

namespace Ui {
class MainWindow;
}

enum class ActiveScene
{
    FirstScene,
    SecondScene,
    GameOverScene,
    NoScene
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    Ui::MainWindow *ui;
    GLWidget* m_glWidget;
    std::shared_ptr<CustomScene> m_scene;
    ActiveScene m_activeScene;
    SettingsManager m_settingsManager;

private:
    void changeScene(std::shared_ptr<CustomScene> scene, const ActiveScene m_activeScene);

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void selectScene();

public slots:
    void sliderValueChangedSlot(int value);
    void glInitializedSlot();
};

#endif // MAINWINDOW_H
