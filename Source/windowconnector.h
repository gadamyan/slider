#ifndef WINDOWCONNECTOR_H
#define WINDOWCONNECTOR_H

#include "Source/Forms/mainwindow.h"
#include "Source/Forms/inputsliderform.h"
#include <QObject>

class WindowConnector : public QObject
{
    Q_OBJECT

private:
    MainWindow m_mainWindow;
    InputSliderForm m_inputSliderForm;

public:
    explicit WindowConnector(QObject *parent = 0);

signals:

public slots:
};

#endif // WINDOWCONNECTOR_H
