#ifndef SLIDER3D_H
#define SLIDER3D_H


#include "Source/Core/glcompositenode.h"
#include "Source/Core/glsprite.h"
#include "Source/Core/glsphere.h"

class Slider3D : public GLCompositeNode
{
private:
    std::shared_ptr<GLSprite> m_sliderThumb;
    std::vector<std::shared_ptr<GLSphere>> m_spheres;

private:
    void scaleBalls();

    void updateThumbPositionAndRotation(const int value);

public:
    Slider3D();
    void setValue(const int value);
};

#endif // SLIDER3D_H
