#include "slider.h"

Slider::Slider()
{
    m_sliderTrackBg = std::make_shared<GLSprite>(":/Resources/sliderTrackBg.png");
    m_sliderTrack = std::make_shared<GLSprite>(":/Resources/sliderTrack.png");
    m_sliderThumb = std::make_shared<GLSprite>(":/Resources/sliderThumb.png");
    m_children.push_back(m_sliderTrackBg);
    m_children.push_back(m_sliderTrack);
    m_children.push_back(m_sliderThumb);
    QVector3D position = m_sliderTrackBg->getPosition();
    position.setX(position.x() + 0.08f);
    m_sliderTrackBg->setPosition(position);
}

void Slider::setValue(const int value)
{
    const QVector2D& trackSize = m_sliderTrack->getContentSize();
    const float positionY = (value / 100.0f - 0.5f) * trackSize.y();

    QVector3D position = m_sliderThumb->getPosition();
    position.setY(positionY);
    m_sliderThumb->setPosition(position);
}

