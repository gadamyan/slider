#ifndef SLIDER_H
#define SLIDER_H

#include "Source/Core/glcompositenode.h"
#include "Source/Core/glsprite.h"

class Slider : public GLCompositeNode
{
private:
    std::shared_ptr<GLSprite> m_sliderTrackBg;
    std::shared_ptr<GLSprite> m_sliderTrack;
    std::shared_ptr<GLSprite> m_sliderThumb;

public:
    Slider();
    void setValue(const int value);
};

#endif // SLIDER_H
