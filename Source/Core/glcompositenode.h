#ifndef GLCOMPOSITENODE_H
#define GLCOMPOSITENODE_H

#include "glnode.h"

class GLCompositeNode : public GLNode
{
protected:
    std::vector<std::shared_ptr<GLNode>> m_children;

public:
    GLCompositeNode();
    virtual void addChild(std::shared_ptr<GLNode> node);
    virtual std::shared_ptr<GLNode> getChildByName(const std::string& name);
    virtual void draw(const QMatrix4x4& projection) override;
};

#endif // GLCOMPOSITENODE_H
