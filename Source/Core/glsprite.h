#ifndef GLSPRITE_H
#define GLSPRITE_H

#include "glnode.h"
#include <QVector2D>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>

class GLSprite : public GLNode
{
private:
    static const float m_textureScale;
    QOpenGLBuffer m_texcoordBuf;
    QOpenGLBuffer m_vertexBuf;
    QOpenGLBuffer m_indexBuf;
    QOpenGLShaderProgram m_program;
    QVector2D m_contentSize;
    QOpenGLTexture *m_texture;
    bool m_canBeDrown;
    size_t m_indicesSize;

private:
    void initTextures(const QString& textureFile);
    void initShaders();
    void initCubeGeometry();

public:
    GLSprite(const QString& textureFile);
    virtual ~GLSprite();
    void draw(const QMatrix4x4& projection) override;
    virtual QVector2D getContentSize() const;
};

#endif // GLSPRITE_H
