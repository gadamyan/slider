#include "glrectgeometry.h"

GLRectGeometry::GLRectGeometry(const QVector2D& contentSize)
{
    const float halfWidth = contentSize.x() * 0.5f;
    const float halfHeight = contentSize.y() * 0.5f;

    m_vertices = {
        QVector3D(-1.0f * halfWidth, -1.0f * halfHeight,  0.0f),
        QVector3D( 1.0f * halfWidth, -1.0f * halfHeight,  0.0f),
        QVector3D(-1.0f * halfWidth,  1.0f * halfHeight,  0.0f),
        QVector3D( 1.0f * halfWidth,  1.0f * halfHeight,  0.0f),
    };

    m_texCoords = {
        QVector2D(0.0f, 0.0f),
        QVector2D(1.0f, 0.0f),
        QVector2D(0.0f, 1.0f),
        QVector2D(1.0f, 1.0f),
    };

    m_indices = {
         0,  1,  2,  3,  3
    };
}
