#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QMatrix4x4>
#include <QBasicTimer>
#include "Source/Core/glscene.h"

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT

private:
    std::shared_ptr<GLScene> m_scene;
    QMatrix4x4 m_projection;
    QBasicTimer m_timer;

public:
    explicit GLWidget(QWidget *parent = 0);
    ~GLWidget();
    void setScene(std::shared_ptr<GLScene> scene);

protected:
    void timerEvent(QTimerEvent *e) override;
    void initializeGL() override;
    void resizeGL(int width, int height) override;
    void paintGL() override;

 signals:
    void glInitializedSignal();
};


#endif // GLWIDGET_H
