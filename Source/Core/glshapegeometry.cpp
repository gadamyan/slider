#include "glshapegeometry.h"

GLShapeGeometry::GLShapeGeometry()
{
}

GLShapeGeometry::~GLShapeGeometry()
{
}

const std::vector<QVector3D>& GLShapeGeometry::getVertices() const
{
    return m_vertices;
}

const std::vector<QVector2D>& GLShapeGeometry::getTexCoords() const
{
    return m_texCoords;
}

const std::vector<GLushort>& GLShapeGeometry::getIndices() const
{
    return m_indices;
}
