#ifndef GLSHAPEGEOMETRY_H
#define GLSHAPEGEOMETRY_H

#include <QVector3D>
#include <QVector2D>
#include <QOpenGLFunctions>

class GLShapeGeometry
{
protected:
    std::vector<QVector3D> m_vertices;
    std::vector<QVector2D> m_texCoords;
    std::vector<GLushort> m_indices;

public:
    GLShapeGeometry();
    virtual ~GLShapeGeometry();
    const std::vector<QVector3D>& getVertices() const;
    const std::vector<QVector2D>& getTexCoords() const;
    const std::vector<GLushort>& getIndices() const;
};

#endif // GLSHAPEGEOMETRY_H
