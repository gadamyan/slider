#include "glsphere.h"
#include "glspheregeometry.h"

//GLSphere GLSprite and GLShaderSprite have lots of duplications.

GLSphere::GLSphere(const float radius, const int slices, const QString& textureFile)
    : m_indexBuf(QOpenGLBuffer::IndexBuffer)
    , m_canBeDrown(true)
    , m_indicesSize(0)
{
    initTextures(textureFile);
    initShaders();
    initGeometry(radius, slices);
}

void GLSphere::initTextures(const QString& textureFile)
{
    QImage image(textureFile);
    if (image.isNull())
    {
        m_canBeDrown = false;
        return;
    }

    m_texture = new QOpenGLTexture(image.mirrored());
    m_texture->setMinificationFilter(QOpenGLTexture::Nearest);
    m_texture->setMagnificationFilter(QOpenGLTexture::Linear);
    m_texture->setWrapMode(QOpenGLTexture::ClampToEdge);
}

void GLSphere::initShaders()
{
    m_canBeDrown &= m_program.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/Shaders/vshader.vsh");
    m_canBeDrown &= m_program.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/Shaders/fshader.fsh");
    m_canBeDrown &= m_program.link();
}

GLSphere::~GLSphere()
{
    m_vertexBuf.destroy();
    m_texcoordBuf.destroy();
    m_indexBuf.destroy();
    //TODO find out what is the issue connected with the deletion of the texture
    //delete m_texture;
}

void GLSphere::initGeometry(const float radius, const int slices)
{
    //TODO break this function into smaller functions
    m_vertexBuf.create();
    m_texcoordBuf.create();
    m_indexBuf.create();

    GLSphereGeometry sphereGeometry(radius, slices);
    const std::vector<QVector3D>& vertices = sphereGeometry.getVertices();
    const std::vector<QVector2D>& texCoords = sphereGeometry.getTexCoords();
    const std::vector<GLushort>& indices = sphereGeometry.getIndices();

    m_vertexBuf.bind();
    m_vertexBuf.allocate(&vertices[0], vertices.size() * sizeof(QVector3D));

    m_texcoordBuf.bind();
    m_texcoordBuf.allocate(&texCoords[0], texCoords.size() * sizeof(QVector2D));

    m_indicesSize = indices.size();
    m_indexBuf.bind();
    m_indexBuf.allocate(&indices[0], indices.size() * sizeof(GLushort));
}

void GLSphere::draw(const QMatrix4x4& projection)
{
    if (!m_canBeDrown || !m_isVisible)
    {
        return;
    }

    m_canBeDrown &= m_program.bind();
    m_texture->bind();

    updateMatrix();
    m_program.setUniformValue("mvp_matrix", projection * m_matrix);
    m_program.setUniformValue("texture", 0);

    m_vertexBuf.bind();
    int vertexLocation = m_program.attributeLocation("a_position");
    m_program.enableAttributeArray(vertexLocation);
    m_program.setAttributeBuffer(vertexLocation, GL_FLOAT, 0, 3, sizeof(QVector3D));

    m_texcoordBuf.bind();
    int texcoordLocation = m_program.attributeLocation("a_texcoord");
    m_program.enableAttributeArray(texcoordLocation);
    m_program.setAttributeBuffer(texcoordLocation, GL_FLOAT, 0, 2, sizeof(QVector2D));

    m_indexBuf.bind();
    glDrawElements(GL_TRIANGLE_STRIP, m_indicesSize, GL_UNSIGNED_SHORT, 0);
}
