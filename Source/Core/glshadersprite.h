#ifndef GLSHADERSPRITE_H
#define GLSHADERSPRITE_H

#include "glnode.h"
#include <QVector2D>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>

class GLShaderSprite : public GLNode
{
private:
    bool m_canBeDrown;
    QOpenGLBuffer m_arrayBuf;
    QOpenGLBuffer m_indexBuf;
    QOpenGLShaderProgram program;
    QVector2D m_contentSize;
    long long m_startingTimestamp;
    size_t m_indicesSize;

private:
    void initShaders(const std::string& vshaderFile, const std::string& fshaderFile);
    void initGeometry();
    long long getTimestamp() const;

public:
    GLShaderSprite(const QVector2D& contentSize, const std::string& vshaderFile, const std::string& fshaderFile);
    virtual ~GLShaderSprite();
    void draw(const QMatrix4x4& projection) override;
    virtual QVector2D getContentSize() const;
};


#endif // GLSHADERSPRITE_H
