#include "glwidget.h"


GLWidget::GLWidget(QWidget *parent) :
    QOpenGLWidget(parent)
{
}

GLWidget::~GLWidget()
{
    // Make sure the context is current when deleting the texture
    // and the buffers.
    makeCurrent();
    doneCurrent();
}

void GLWidget::setScene(std::shared_ptr<GLScene> scene)
{
    m_scene = scene;
}

void GLWidget::timerEvent(QTimerEvent *)
{
    update();
}

void GLWidget::initializeGL()
{
    initializeOpenGLFunctions();

    glClearColor(0, 0, 0, 1);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER, 0.3);
    glInitializedSignal();
    m_timer.start(12, this);
}


void GLWidget::resizeGL(int width, int height)
{
    //TODO make these values configurable
    qreal aspectRatio = qreal(width) / qreal(height ? height : 1);
    const qreal zNear = 1.0, zFar = 7.0, fieldOfView = 45.0;
    m_projection.setToIdentity();
    m_projection.perspective(fieldOfView, aspectRatio, zNear, zFar);
}

void GLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (m_scene)
    {
        m_scene->draw(m_projection);
    }
}
