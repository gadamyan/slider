#ifndef GLSPHERE_H
#define GLSPHERE_H

#include "glnode.h"
#include <QVector2D>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>

class GLSphere : public GLNode
{
private:
    QOpenGLBuffer m_texcoordBuf;
    QOpenGLBuffer m_vertexBuf;
    QOpenGLBuffer m_indexBuf;
    QOpenGLShaderProgram m_program;
    QVector2D m_contentSize;
    QOpenGLTexture *m_texture;
    bool m_canBeDrown;
    size_t m_indicesSize;

private:
    void initTextures(const QString& textureFile);
    void initShaders();
    void initGeometry(const float radius, const int slices);

public:
    GLSphere(const float radius, const int slices, const QString& textureFile);
    virtual ~GLSphere();
    void draw(const QMatrix4x4& projection) override;
};

#endif // GLSPHERE_H
