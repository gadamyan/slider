#ifndef GLRECTGEOMETRY_H
#define GLRECTGEOMETRY_H

#include "glshapegeometry.h"
#include <QVector3D>
#include <QVector2D>
#include <QOpenGLFunctions>

class GLRectGeometry : public GLShapeGeometry
{
public:
    GLRectGeometry(const QVector2D& contentSize);
};

#endif // GLRECTGEOMETRY_H
