#include "glshadersprite.h"
#include "glrectgeometry.h"
#include <chrono>

//GLSphere GLSprite and GLShaderSprite have lots of duplications.

GLShaderSprite::GLShaderSprite(const QVector2D& contentSize, const std::string& vshaderFile, const std::string& fshaderFile)
    : m_canBeDrown(true)
    , m_indexBuf(QOpenGLBuffer::IndexBuffer)
    , m_contentSize(contentSize)
    , m_startingTimestamp(getTimestamp())
    , m_indicesSize(0)
{
    initShaders(vshaderFile, fshaderFile);
    initGeometry();
}

void GLShaderSprite::initShaders(const std::string& vshaderFile, const std::string& fshaderFile)
{
    m_canBeDrown &= program.addShaderFromSourceFile(QOpenGLShader::Vertex, vshaderFile.c_str());
    m_canBeDrown &= program.addShaderFromSourceFile(QOpenGLShader::Fragment, fshaderFile.c_str());
    m_canBeDrown &= program.link();
}

GLShaderSprite::~GLShaderSprite()
{
    m_arrayBuf.destroy();
    m_indexBuf.destroy();
}

long long GLShaderSprite::getTimestamp() const
{
    using namespace std::chrono;
    return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}

void GLShaderSprite::initGeometry()
{
    m_arrayBuf.create();
    m_indexBuf.create();

    GLRectGeometry rectGeometry(m_contentSize);
    const std::vector<QVector3D>& vertices = rectGeometry.getVertices();
    const std::vector<GLushort>& indices = rectGeometry.getIndices();

    m_arrayBuf.bind();
    m_arrayBuf.allocate(&vertices[0], vertices.size() * sizeof(QVector3D));

    m_indicesSize = indices.size();
    m_indexBuf.bind();
    m_indexBuf.allocate(&indices[0], m_indicesSize * sizeof(GLushort));
}

void GLShaderSprite::draw(const QMatrix4x4& projection)
{
    if (!m_canBeDrown || !m_isVisible)
    {
        return;
    }

    m_canBeDrown &= program.bind();

    updateMatrix();

    program.setUniformValue("mvp_matrix", projection * m_matrix);

    const long long currentTimestamp = getTimestamp();
    const float passedTime = static_cast<float>(currentTimestamp - m_startingTimestamp);

    program.setUniformValue("time", passedTime);
    program.setUniformValue("resolution", m_contentSize);


    m_arrayBuf.bind();
    const int vertexLocation = program.attributeLocation("a_position");
    program.enableAttributeArray(vertexLocation);
    program.setAttributeBuffer(vertexLocation, GL_FLOAT, 0, 3, sizeof(QVector3D));

    m_indexBuf.bind();
    glDrawElements(GL_TRIANGLE_STRIP, m_indicesSize, GL_UNSIGNED_SHORT, 0);
}

QVector2D GLShaderSprite::getContentSize() const
{
    return m_contentSize;
}
