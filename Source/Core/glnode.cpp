#include "glnode.h"

GLNode::GLNode()
    : m_scale(1)
    , m_isVisible(true)
{
    initializeOpenGLFunctions();
}

GLNode::~GLNode()
{
}

void GLNode::draw(const QMatrix4x4& projection)
{
}

void GLNode::updateMatrix()
{
    m_matrix.setToIdentity();
    m_matrix.translate(m_position.x() , m_position.y(), m_position.z());
    m_matrix.rotate(m_rotation);
    m_matrix.scale(m_scale);
}

void GLNode::setName(const std::string& name)
{
    m_name = name;
}

std::string GLNode::getName() const
{
    return m_name;
}

void GLNode::setVisible(const bool isVisible)
{
    m_isVisible = isVisible;
}

bool GLNode::isVisible() const
{
    return m_isVisible;
}

void GLNode::setRotation(const QQuaternion& rotation)
{
    m_rotation = rotation;
}

QQuaternion GLNode::getRotation() const
{
    return m_rotation;
}

void GLNode::setPosition(const QVector3D& position)
{
    m_position = position;
}

QVector3D GLNode::getPosition() const
{
    return m_position;
}

void GLNode::setScale(const float scale)
{
    m_scale = scale;
}

float GLNode::getScale() const
{
    return m_scale;
}
