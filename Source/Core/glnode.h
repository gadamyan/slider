#ifndef GLNODE_H
#define GLNODE_H

#include <QVector3D>
#include <QMatrix4x4>
#include <QOpenGLFunctions>
#include <QQuaternion>

class GLNode : protected QOpenGLFunctions
{
protected:
    QMatrix4x4 m_matrix;
    QVector3D m_position;
    QQuaternion m_rotation;
    std::string m_name;
    float m_scale;
    bool m_isVisible;

protected:
    virtual void updateMatrix();

public:
    GLNode();
    virtual ~GLNode();
    virtual void setName(const std::string& name);
    virtual std::string getName() const;
    virtual void setVisible(const bool isVisible);
    virtual bool isVisible() const;
    virtual void draw(const QMatrix4x4& projection);
    virtual void setRotation(const QQuaternion& rotation);
    virtual QQuaternion getRotation() const;
    virtual void setPosition(const QVector3D& position);
    virtual QVector3D getPosition() const;
    virtual void setScale(const float scale);
    virtual float getScale() const;
};

#endif // GLNODE_H
