#include "glcompositenode.h"
#include <cassert>

GLCompositeNode::GLCompositeNode()
{

}

void GLCompositeNode::addChild(std::shared_ptr<GLNode> node)
{
    assert(node != nullptr && "Node shouldn't be null");
    const auto it = std::find(m_children.cbegin(), m_children.cend(), node);
    assert(it == m_children.cend() && "The node should not be in the container");
    m_children.push_back(node);
}

std::shared_ptr<GLNode> GLCompositeNode::getChildByName(const std::string& name)
{
    const auto it = std::find_if(m_children.cbegin(), m_children.cend(),
                              [name](const std::shared_ptr<GLNode>& node)
    {
        return node->getName() == name;
    });
    if (it != m_children.cend())
    {
        return *it;
    }
    else
    {
        return std::shared_ptr<GLNode>();
    }
}

void GLCompositeNode::draw(const QMatrix4x4& projection)
{
    updateMatrix();
    for (auto rit = m_children.rbegin(); rit != m_children.rend(); ++rit)
    {
        (*rit)->draw(projection * m_matrix);
    }
}
