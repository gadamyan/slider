#include "glspheregeometry.h"
#include <cmath>

GLSphereGeometry::GLSphereGeometry(const float radius, const int slices)
{
    const int stacks = slices;

    for (int stackNumber = 0; stackNumber <= stacks; ++stackNumber)
    {
        for (int sliceNumber = 0; sliceNumber <= slices; ++sliceNumber)
        {
            const float theta = (float) (stackNumber * M_PI / stacks);
            const float phi = (float) (sliceNumber * 2 * M_PI / slices);
            const float sinTheta = sin(theta);
            const float sinPhi = sin(phi);
            const float cosTheta = cos(theta);
            const float cosPhi = cos(phi);

            const float nx = cosPhi * sinTheta;
            const float ny = cosTheta;
            const float nz = sinPhi * sinTheta;


            const float x = radius * nx;
            const float y = radius * ny;
            const float z = radius * nz;

            const float u = 1.f - ((float)sliceNumber / (float)slices);
            const float v = (float)stackNumber / (float)stacks;

            m_vertices.push_back(QVector3D(x, y, z));
            m_texCoords.push_back(QVector2D(u, v));
        }
    }

    for (int stackNumber = 0; stackNumber < stacks; ++stackNumber)
    {
        for (int sliceNumber = 0; sliceNumber < slices; ++sliceNumber)
        {
            const short second = static_cast<short>((sliceNumber * (stacks + 1)) + stackNumber);
            const short first = static_cast<short>(second + stacks + 1);

            m_indices.push_back(first);
            m_indices.push_back(second);
            m_indices.push_back((first + 1));

            m_indices.push_back(second);
            m_indices.push_back((second + 1));
            m_indices.push_back((first + 1));
        }
    }
}
