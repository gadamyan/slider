#ifndef GLSPHEREGEOMETRY_H
#define GLSPHEREGEOMETRY_H

#include "glshapegeometry.h"
#include <QVector3D>
#include <QVector2D>
#include <QOpenGLFunctions>

class GLSphereGeometry : public GLShapeGeometry
{
public:
    GLSphereGeometry(const float radius, const int slices);
};

#endif // GLSPHEREGEOMETRY_H
