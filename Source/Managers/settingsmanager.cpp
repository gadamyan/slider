#include "settingsmanager.h"
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>

SettingsManager::SettingsManager()
{
    const QString& jsonStr = loadSettingsJson();
    parseSettings(jsonStr);
}

const QString SettingsManager::loadSettingsJson() const
{
    QFile file;
    file.setFileName(":/Resources/Settings.json");
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QString jsonStr = file.readAll();
    file.close();
    return jsonStr;
}

void SettingsManager::parseSettings(const QString& jsonStr)
{
    const QJsonDocument& document = QJsonDocument::fromJson(jsonStr.toUtf8());
    const QJsonObject& settingsJson = document.object();

    const QJsonValue& firstSceneSettingsJson = settingsJson["FirstScene"];
    m_settingsData.firstSceneData = parseSceneSettings(firstSceneSettingsJson.toObject());

    const QJsonValue& secondSceneSettingsJson = settingsJson["SecondScene"];
    m_settingsData.secondSceneData = parseSceneSettings(secondSceneSettingsJson.toObject());
}

const SceneSettingsData SettingsManager::parseSceneSettings(const QJsonObject& sceneSettingsJson) const
{
    const QJsonValue& targetZoneEnteringCountJson = sceneSettingsJson["TargetZoneEnteringCount"];
    const int targetZoneEnteringCount = targetZoneEnteringCountJson.toInt(0);

    const QJsonValue& lowerTargetZoneJson = sceneSettingsJson["LowerTargetZone"];
    const int lowerTargetZone = lowerTargetZoneJson.toInt(0);

    const QJsonValue& upperTargetZoneJson = sceneSettingsJson["UpperTargetZone"];
    const int upperTargetZone = upperTargetZoneJson.toInt(0);

    return SceneSettingsData { targetZoneEnteringCount, lowerTargetZone, upperTargetZone };
}

const SceneSettingsData& SettingsManager::getFirstSceneSettingsData() const
{
    return m_settingsData.firstSceneData;
}

const SceneSettingsData& SettingsManager::getSecondSceneSettingsData() const
{
    return m_settingsData.secondSceneData;
}
