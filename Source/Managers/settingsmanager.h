#ifndef SETTINGSMANAGER_H
#define SETTINGSMANAGER_H

#include "settingsdata.h"
#include <QObject>

class SettingsManager
{
private:
    SettingsData m_settingsData;

private:
    void parseSettings(const QString& jsonStr);
    const SceneSettingsData parseSceneSettings(const QJsonObject& sceneSettingsJson) const;
    const QString loadSettingsJson() const;

public:
    SettingsManager();
    const SceneSettingsData& getFirstSceneSettingsData() const;
    const SceneSettingsData& getSecondSceneSettingsData() const;
};

#endif // SETTINGSMANAGER_H
