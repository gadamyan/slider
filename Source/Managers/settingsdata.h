#ifndef SETTINGSDATA_H
#define SETTINGSDATA_H

struct SceneSettingsData
{
    int targetZoneEnteringCount;
    int lowerTargetZone;
    int upperTargetZone;
};

struct SettingsData
{
    SceneSettingsData firstSceneData;
    SceneSettingsData secondSceneData;
};

#endif // SETTINGSDATA_H
