#ifndef FIRSTSCENE_H
#define FIRSTSCENE_H

#include "customscene.h"
#include "Source/slider.h"
#include "Source/Core/glshadersprite.h"
#include "Source/Managers/settingsdata.h"

class FirstScene : public CustomScene
{
private:
    std::shared_ptr<GLShaderSprite> m_effectSprite;
    std::shared_ptr<Slider> m_slider;
    std::shared_ptr<GLSprite> m_sceneBg;
    const SceneSettingsData m_sceneSettingsData;
    bool m_wasInTargetZone;
    int m_targetZoneEnteringCounter;

public:
    FirstScene(const SceneSettingsData& sceneSettingsData);

public:
    void sliderValueChanged(const int value) override;
    bool shouldChangeScene() override;
};

#endif // FIRSTSCENE_H
