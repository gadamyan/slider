#ifndef SECONDSCENE_H
#define SECONDSCENE_H


#include "customscene.h"
#include "Source/slider3D.h"
#include "Source/Core/glshadersprite.h"
#include "Source/Managers/settingsdata.h"

class SecondScene : public CustomScene
{
private:
    std::shared_ptr<GLShaderSprite> m_effectSprite;
    std::shared_ptr<Slider3D> m_slider;
    std::shared_ptr<GLSprite> m_sceneBg;
    const SceneSettingsData m_sceneSettingsData;
    bool m_wasInTargetZone;
    int m_targetZoneEnteringCounter;

public:
    SecondScene(const SceneSettingsData& sceneSettingsData);

public:
    void sliderValueChanged(const int value) override;
    bool shouldChangeScene() override;
};

#endif // SECONDSCENE_H
