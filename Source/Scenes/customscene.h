#ifndef CUSTOMSCENE_H
#define CUSTOMSCENE_H

#include "Source/Core/glscene.h"

class CustomScene : public GLScene
{
public:
    CustomScene();
    virtual void sliderValueChanged(const int value);
    virtual bool shouldChangeScene();
};

#endif // CUSTOMSCENE_H
