#include "secondscene.h"
#include "Source/Core/glsprite.h"

//TODO SecondScene and first scene have similar code, remove duplications

SecondScene::SecondScene(const SceneSettingsData& sceneSettingsData)
    : m_sceneSettingsData(sceneSettingsData)
    , m_wasInTargetZone(false)
    , m_targetZoneEnteringCounter(0)
{
    auto sceneBg = std::make_shared<GLSprite>(":/Resources/firstSceneBg.jpg");
    m_sceneBg = sceneBg;
    m_children.push_back(sceneBg);

    m_effectSprite = std::make_shared<GLShaderSprite>(QVector2D(8.0f, 8.0f),
                     ":/Shaders/effectVshader.vsh", ":/Shaders/effectFshader.fsh");
    m_children.push_back(m_effectSprite);
    m_effectSprite->setVisible(false);

    m_slider = std::make_shared<Slider3D>();

    QVector3D position = m_slider->getPosition();
    position.setY(position.y() - 1.0f);
    m_slider->setPosition(position);

    m_children.push_back(m_slider);
}

void SecondScene::sliderValueChanged(int value)
{
    m_slider->setValue(value);
    const int lowerTargetZone = m_sceneSettingsData.lowerTargetZone;
    const int upperTargetZone = m_sceneSettingsData.upperTargetZone;
    bool isInTargetZone = value < lowerTargetZone || value > upperTargetZone;
    if (!m_wasInTargetZone && isInTargetZone)
    {
        ++m_targetZoneEnteringCounter;
        m_effectSprite->setVisible(true);
    }
    if (m_wasInTargetZone && !isInTargetZone)
    {
        m_effectSprite->setVisible(false);
    }
    m_wasInTargetZone = isInTargetZone;
}

bool SecondScene::shouldChangeScene()
{
    return m_targetZoneEnteringCounter >= m_sceneSettingsData.targetZoneEnteringCount;
}
