#include "gameoverscene.h"
#include "Source/Core/glsprite.h"

GameOverScene::GameOverScene()
{
    auto sceneBg = std::make_shared<GLSprite>(":/Resources/gameOver.jpg");
    m_children.push_back(sceneBg);
}
