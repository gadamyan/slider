#include "firstscene.h"
#include "Source/Core/glsprite.h"

//TODO SecondScene and first scene have similar code, remove duplications


FirstScene::FirstScene(const SceneSettingsData& sceneSettingsData)
    : m_sceneSettingsData(sceneSettingsData)
    , m_wasInTargetZone(false)
    , m_targetZoneEnteringCounter(0)
{
    auto sceneBg = std::make_shared<GLSprite>(":/Resources/firstSceneBg.jpg");
    m_sceneBg = sceneBg;
    m_children.push_back(sceneBg);

    m_effectSprite = std::make_shared<GLShaderSprite>(QVector2D(8.0f, 8.0f),
                     ":/Shaders/effectVshader.vsh", ":/Shaders/effectFshader.fsh");
    m_children.push_back(m_effectSprite);
    m_effectSprite->setVisible(false);

    m_slider = std::make_shared<Slider>();
    m_children.push_back(m_slider);
}

void FirstScene::sliderValueChanged(int value)
{
    m_slider->setValue(value);
    const int lowerTargetZone = m_sceneSettingsData.lowerTargetZone;
    const int upperTargetZone = m_sceneSettingsData.upperTargetZone;
    bool isInTargetZone = value < lowerTargetZone || value > upperTargetZone;
    if (!m_wasInTargetZone && isInTargetZone)
    {
        ++m_targetZoneEnteringCounter;
        m_effectSprite->setVisible(true);
    }
    if (m_wasInTargetZone && !isInTargetZone)
    {
        m_effectSprite->setVisible(false);
    }
    m_wasInTargetZone = isInTargetZone;
}

bool FirstScene::shouldChangeScene()
{
    return m_targetZoneEnteringCounter >= m_sceneSettingsData.targetZoneEnteringCount;
}
