#ifndef GAMEOVERSCENE_H
#define GAMEOVERSCENE_H

#include "customscene.h"

class GameOverScene : public CustomScene
{
public:
    GameOverScene();
};

#endif // GAMEOVERSCENE_H
