#include "windowconnector.h"

WindowConnector::WindowConnector(QObject *parent) : QObject(parent)
{
    m_mainWindow.show();
    m_inputSliderForm.show();
    QObject::connect(&m_inputSliderForm, SIGNAL(sliderValueChangedSignal(int)),
                     &m_mainWindow, SLOT(sliderValueChangedSlot(int)));
}
